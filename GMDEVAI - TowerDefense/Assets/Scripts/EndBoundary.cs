﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndBoundary : MonoBehaviour {

    private Health health;

    // Use this for initialization
    void Start () {
        health = GetComponent<Health>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Collided with end boundary.");
        Health health = gameObject.GetComponent<Health>();
        Damage damage = other.gameObject.GetComponent<Damage>();

        if (other.gameObject.layer == 8) // If collide with enemy
        {
            health.TakeDamage(damage.DamageValue);
            other.gameObject.SetActive(false);

            if (health.CurrentHP <= 0)
            {
                SceneHandler.LoadGameOverScene();
            }
        }
    }
}
