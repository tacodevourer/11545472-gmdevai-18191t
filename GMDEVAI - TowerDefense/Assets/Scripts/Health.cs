﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public float MaxHp;
    public float CurrentHP;
    public Text LivesText;

    void Start()
    {
        CurrentHP = MaxHp;
    }

    void Update()
    {
        // If gameObject is the player (lives need to be displayed)
        if (gameObject.layer == 9)
        DisplayLives();
    }

    public void TakeDamage(float damage)
    {
        CurrentHP -= damage;

        if (CurrentHP <= 0)
        {
            SceneHandler.LoadGameOverScene();
            gameObject.SetActive(false);
        }
    }

    public void TakeHealing(float healValue)
    {
        CurrentHP += healValue;

        if (CurrentHP >= MaxHp)
        {
            CurrentHP = MaxHp;
        }
    }

    public void DisplayLives()
    {
        LivesText.text = "Lives: " + CurrentHP.ToString();
    }
}
