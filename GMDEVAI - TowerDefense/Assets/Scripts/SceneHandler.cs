﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneHandler : MonoBehaviour {

    void Awake()
    {
#if UNITY_STANDALONE
        Screen.SetResolution(800, 600, false);
        Screen.fullScreen = false;
#endif    
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    public static void LoadGameOverScene()
    {
        SceneManager.LoadScene("GameOverScene");
    }
}
