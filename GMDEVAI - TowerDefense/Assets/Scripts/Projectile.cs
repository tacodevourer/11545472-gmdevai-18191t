﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnDeath : UnityEvent<Projectile> { }

public class Projectile : MonoBehaviour
{
    public float projectileDamage;
    public float projectileSpeed;

    public OnDeath OnDeath;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Projectile hit.");
        Health health = collider.gameObject.GetComponent<Health>();

        // If projectile hits anything with a health component
        if(health)
        {
            health.TakeDamage(projectileDamage);
            this.gameObject.SetActive(false);
            Debug.Log("Projectile hit.");
        }
        else OnDeath.Invoke(this);
    }

    private IEnumerator Deactivate(float duration)
    {
        yield return new WaitForSeconds(duration);
        OnDeath.Invoke(this);
    }
}
