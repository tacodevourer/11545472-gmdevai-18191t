﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    public float rateofFire = 0.5f;

    public Transform projectileSpawn;
    public GameObject projectile;
    public GameObject instantiatedObj;
    public List<GameObject> spawned = new List<GameObject>();
    public List<GameObject> pooledObjects = new List<GameObject>();

    private GameObject target;
    private Quaternion projectileRotation;
    private float elapsedTime;

    // Use this for initialization
    void Start () {
        target = null;
	}
	
	// Update is called once per frame
	void Update () {
        elapsedTime += Time.deltaTime;

        // If there is a target in range, keep rotating tower
        if(target != null)
        {
            RotateTower();
            ShootProjectile();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Entered tower range.");

        // If object collides with enemy
        if (other.gameObject.layer == 8)
        {
            target = other.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        // Reset target when object leaves the collider
        target = null;
    }

    void RotateTower()
    {
        // Rotate tower to face target
        gameObject.transform.LookAt(target.transform);

        // Rotate projectile to match tower rotation;
        projectileRotation = gameObject.transform.rotation;
        // Debug.Log(projectileRotation);
    }

    void ShootProjectile()
    {
        if (rateofFire <= elapsedTime)
        {
            // Reset shoot cooldown
            elapsedTime = 0.0f;

            if (pooledObjects.Count > 0)
            {
                instantiatedObj = pooledObjects[0];
                pooledObjects.RemoveAt(0);
                instantiatedObj.transform.position = projectileSpawn.position;
                instantiatedObj.transform.rotation = projectileRotation;
                instantiatedObj.SetActive(true);
            }
            else
            {
                // Instantiate projectile
                instantiatedObj = Instantiate(projectile, projectileSpawn.position, projectileRotation);
            }

            // Add velocity using projectile direction and speed
            instantiatedObj.GetComponent<Rigidbody2D>().velocity = projectile.transform.up * projectile.GetComponent<Projectile>().projectileSpeed;
            Debug.Log("Shot projectile");

            Projectile spawnedProjectile = instantiatedObj.GetComponent<Projectile>();
            spawnedProjectile.OnDeath.AddListener(OnDeath);

        }
    }

    void OnDeath(Projectile projectile)
    {
        // Pool the object instead of destroying it
        spawned.Remove(instantiatedObj);
        projectile.gameObject.SetActive(false);
        pooledObjects.Add(projectile.gameObject);
        projectile.OnDeath.RemoveListener(OnDeath);
    }
}
