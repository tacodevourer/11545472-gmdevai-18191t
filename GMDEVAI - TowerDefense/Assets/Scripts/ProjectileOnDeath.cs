﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileOnDeath : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Projectile projectile = GetComponent<Projectile>();
        projectile.OnDeath.AddListener(OnDeath);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnDeath(Projectile projectile)
    {
        gameObject.SetActive(false);
    }
}
