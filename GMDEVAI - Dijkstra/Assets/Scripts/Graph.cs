﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Graph : MonoBehaviour {

    [SerializeField]
    protected List<Node> nodes = new List<Node>();

    public virtual List<Node> getNodes
    {
        get { return nodes; }
    }

    public virtual Path GetDijkstraPath(Node start, Node end)
    {
        // Check if arguments are not null
        if (start == null || end == null)
        {
            throw new ArgumentNullException();
        }

        // Create final path
        Path path = new Path();

        // If the start and end node nodes are the same, we can return the start node
        if (start == end)
        {
            path.getNodes.Add(start);
            return path;
        }

        // Create a list of unvisited nodes
        List<Node> unvisited = new List<Node>();

        //Previous nodes in optimal path from source
        Dictionary<Node, Node> previous = new Dictionary<Node, Node>();

        // Create a dictionary to store the calculated distances per node
        Dictionary<Node, float> distances = new Dictionary<Node, float>();

        // Set all nodes to infinity except start node
        for (int i = 0; i < nodes.Count; i++)
        {
            Node node = nodes[i];
            // Add all nodes to unvisited
            unvisited.Add(node);

            // Setting the distance of each node to infinity
            distances.Add(node, float.MaxValue);
        }

        // --- DIJKSTRA STEP 1: SET START NODE TO 0 ---

        // Set the starting node distance to 0 ("start" is the key, which corresponds to a float)
        distances[start] = 0f;

        // While there are still remaining nodes in the unvisited list
        while(unvisited.Count != 0)
        {
            // Sort list of unvisited nodes by distance, smallest distance at start, largest at end
            unvisited = unvisited.OrderBy(node => distances[node]).ToList();

            // Get the node with the smallest distance (first element in the now-sorted list)
            Node current = unvisited[0];

            // Remove the current node from the list of unvisited nodes
            unvisited.Remove(current);

            if(current == end)
            {
                // Construct the shortest path
                while(previous.ContainsKey(current))
                {
                    // Insert the node into the final result path
                    path.getNodes.Insert(0, current);

                    // Traverse from start to end (replaces current node)
                    current = previous[current];

                    // Insert the source onto the final result
                    path.getNodes.Insert(0, current);
                    break;
                }
            }
            // --- DIJKSTRA STEP 2: COMPUTE NODE COSTS --- 

            // Looping through the node connections (neighbor nodes) and where the connection is available at unvisited list
            for (int i = 0; i < current.getConnections.Count; i++)
            {
                Node neighbor = current.getConnections[i];

                // Getting the distance between the current node and the neighbor node
                float length = Vector3.Distance(current.transform.position, neighbor.transform.position);

                // The distance from start node to this connection of the current node
                float alt = distances[current] + length;

                // --- DIJKSTRA STEP 3: LOOK FOR CHEAPEST COST ---

                // If a shorter path to the connection has been found
                if (alt < distances[neighbor])
                {
                    distances[neighbor] = alt;
                    previous[neighbor] = current;
                }
            }
        }

        // When no remaining nodes left unvisited, return final path
        path.Bake();
        return path;
    }
}
